# cmd2exe

![](https://img.shields.io/badge/written%20in-bash-blue)

Shell script to create an .exe wrapper for a windows command.

The Windows taskbar is grouped by target exe. By creating wrapper exe files, you can pin individual documents and shortcuts.

## About

`Usage: cmd2exe 'windows command' [icon.ico] output.exe`

Requires cygwin with the mingw-w64-gcc and binutils packages.

## Known Issues

"File not recognised" under Cygwin64 when adding an icon

- This issue is caused by the binutils package producing a 64-bit resource that is incompatible with the 32-bit binary object from gcc. You can resolve this issue by installing the `mingw64-i686-binutils` package, and replacing calls to `windres` with `i686-w64-mingw32-windres`. (Thanks A.C. for the report)

## Changelog

2017-02-11
- Document known issue

2015-02-06 r01
- Initial release
- [⬇️ cmd2exe_r01.tar.gz](dist-archive/cmd2exe_r01.tar.gz) *(1.07 KiB)*

